package com.qmetry.qaf.example.pages;

import org.hamcrest.Matchers;

import com.qmetry.qaf.automation.core.ConfigurationManager;
import com.qmetry.qaf.automation.core.QAFTestBase;
import com.qmetry.qaf.automation.step.CommonStep;
import com.qmetry.qaf.automation.ui.WebDriverBaseTestPage;
import com.qmetry.qaf.automation.ui.api.PageLocator;
import com.qmetry.qaf.automation.ui.api.WebDriverTestPage;
import com.qmetry.qaf.automation.util.Reporter;
import com.qmetry.qaf.automation.util.Validator;

public class NestLoginPage extends WebDriverBaseTestPage<WebDriverTestPage> {

	@Override
	protected void openPage(PageLocator locator, Object... args) {

		driver.get("/");
		

	}

	public void enterCredentials(String username, String password) {
		try {
			Reporter.log("Data received: username" + username + "password" + password);
			CommonStep.sendKeys(username, "username.loc");
			CommonStep.sendKeys(password, "password.loc");
		} catch (Exception e) {
			Reporter.log("exception occured" + e.toString());
		}

	}

	public void clickOnLogIn() throws InterruptedException {
		CommonStep.click("logInbtn.loc");
		driver.manage().window().maximize();

	}

	public void verifyLogInPage() throws InterruptedException {
		Validator.verifyThat("title", driver.getTitle(), Matchers.containsString("Infostretch NEST"));

		// Validator.verifyThat(driver.getCurrentUrl().contains("login"),
		// Matchers.equalTo(true));

		// ConfigurationManager.getBundle().getString("headers.value");

	}

	public void verifyLogInErrorWhenEmptyCredentials() throws InterruptedException {
		String actual = "Username is required";
		String expected = CommonStep.getText("blankUserNameError.loc");
		Validator.verifyThat(actual, Matchers.equalTo(expected));

	}

	@Override
	public void waitForPageToLoad() {
		super.waitForPageToLoad();
	}

}
