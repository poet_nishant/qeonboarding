package com.qmetry.qaf.example.pages;

import static com.qmetry.qaf.automation.step.CommonStep.click;
import static com.qmetry.qaf.automation.step.CommonStep.waitForNotVisible;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.qmetry.qaf.automation.ui.WebDriverBaseTestPage;
import com.qmetry.qaf.automation.ui.api.PageLocator;
import com.qmetry.qaf.automation.ui.api.WebDriverTestPage;
import com.qmetry.qaf.automation.ui.webdriver.QAFExtendedWebElement;
import com.qmetry.qaf.automation.ui.webdriver.QAFWebElement;
import com.qmetry.qaf.example.utility.CommonUtils;

public class HomePage extends WebDriverBaseTestPage<WebDriverTestPage> {

	WebDriverWait wait = new WebDriverWait(driver, 120);
	public static final String LOADER = "Login.Loader.img";

	public void waitForLocToBeClickable(QAFWebElement element) {
		wait.until(ExpectedConditions.elementToBeClickable(element));
	}

	public void waitForPageLoader() {

		waitForNotVisible(LOADER, 120);
		waitForLocToBeClickable(new QAFExtendedWebElement("logOutbtn.loc"));
	}

	@Override
	protected void openPage(PageLocator locator, Object... args) {
		// Not doing anything because it is opened by previous click
	}

	public void clickOnLogOut() throws InterruptedException {

		waitForPageLoader();
		QAFExtendedWebElement element = new QAFExtendedWebElement("logOutbtn.loc");
		JavascriptExecutor js = (JavascriptExecutor) driver;
		js.executeScript("arguments[0].click();", element);
		// CommonStep.click("logOutbtn.loc");

	}

	public void clickVerifyAll() throws InterruptedException {

	}

	public void switchManagerView() throws InterruptedException {
		waitForNotVisible(LOADER, 60);
		click("myview.slider");
	}

	public void clickTravelTab() throws InterruptedException {
		waitForNotVisible(LOADER, 60);
		click("toogle.menu");
		Thread.sleep(10000);
		CommonUtils.openSliderMenu("Travel", "Travel Requests", "Manager");

		// Thread.sleep(10000);
	}

	

	public void verifyAction() {

	}

}
