package com.qmetry.qaf.example.utility;

import org.hamcrest.Matcher;
import org.hamcrest.Matchers;

import com.qmetry.qaf.automation.core.ConfigurationManager;
import com.qmetry.qaf.automation.ui.webdriver.QAFExtendedWebElement;
import com.qmetry.qaf.automation.util.Validator;

public class CommonUtils {

	public static void openSliderMenu(String menu, String subMenu, String view) throws InterruptedException {

		QAFExtendedWebElement Menu;
		// open menu first
		if (view.equalsIgnoreCase("Manager")) {
			Menu = new QAFExtendedWebElement(
					String.format(ConfigurationManager.getBundle().getPropertyValue("menu.managerview"), menu));
			// js scroll
		} else {
			Menu = new QAFExtendedWebElement(
					String.format(ConfigurationManager.getBundle().getPropertyValue("menu.myview"), menu));

		}
		Menu.click();

		// open submenu then
		Thread.sleep(10000);

		QAFExtendedWebElement subMenuItem = new QAFExtendedWebElement(
				String.format(ConfigurationManager.getBundle().getPropertyValue("menu.subheader"), subMenu));
		// js scroll
		subMenuItem.click();
	}

	public static void verifyPresent(String str) {

		// Window scroll

		QAFExtendedWebElement btn = new QAFExtendedWebElement(
				String.format(ConfigurationManager.getBundle().getPropertyValue("button.approve"), str));

		 Validator.verifyThat(btn.getText(),Matchers.equalTo(str));
	}

}
