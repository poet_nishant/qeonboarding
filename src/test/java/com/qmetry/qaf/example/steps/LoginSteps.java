package com.qmetry.qaf.example.steps;

import org.apache.velocity.test.BaseTestCase;

import com.qmetry.qaf.automation.core.QAFTestBase;
import com.qmetry.qaf.automation.step.CommonStep;
import com.qmetry.qaf.automation.step.QAFTestStep;
import com.qmetry.qaf.automation.ui.WebDriverBaseTestPage;
import com.qmetry.qaf.automation.ui.api.PageLocator;
import com.qmetry.qaf.automation.ui.api.WebDriverTestPage;
import com.qmetry.qaf.example.pages.NestLoginPage;

public class LoginSteps extends WebDriverBaseTestPage<WebDriverTestPage> {

	NestLoginPage loginPage = new NestLoginPage();

	@QAFTestStep(description = "user launches nest site")
	public void openBrowser() {

		loginPage.launchPage(null);

	}

	// This test method provide data from csv. Also tells test should not take more
	// than 10 second and can put threshold=10.
	// @QAFDataProvider(dataFile = "resources/data/logintestdata.csv") It is for
	// java TestNG.
	@QAFTestStep(description = "user login with username {0} and password {1}")
	public void login(String userName, String password) throws InterruptedException {
		loginPage.enterCredentials(userName, password);
		loginPage.clickOnLogIn();
	}

//	@QAFTestStep(description = "user navigates to Home page")
//	public void user_NavigateHome() throws InterruptedException {
//		loginPage.clickOnLogIn();
//
//	}

	@QAFTestStep(description = "user get error username is required")
	public void user_NavigateHome() throws InterruptedException {
		loginPage.verifyLogInErrorWhenEmptyCredentials();
	}

	@QAFTestStep(description = "verify user logout successfully")
	public void logOutVerifictaion() throws InterruptedException {
		loginPage.verifyLogInPage();
	}

	@Override
	protected void openPage(PageLocator locator, Object... args) {

	}

}
