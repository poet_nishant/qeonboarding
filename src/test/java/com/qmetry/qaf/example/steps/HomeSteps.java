package com.qmetry.qaf.example.steps;

import static com.qmetry.qaf.automation.step.CommonStep.click;
import static com.qmetry.qaf.automation.step.CommonStep.waitForNotVisible;

import java.util.List;

import org.openqa.selenium.support.ui.WebDriverWait;

import com.qmetry.qaf.automation.step.QAFTestStep;
import com.qmetry.qaf.automation.ui.WebDriverBaseTestPage;
import com.qmetry.qaf.automation.ui.annotations.FindBy;
import com.qmetry.qaf.automation.ui.api.PageLocator;
import com.qmetry.qaf.automation.ui.api.WebDriverTestPage;
import com.qmetry.qaf.automation.ui.webdriver.QAFWebElement;
import com.qmetry.qaf.example.pages.HomePage;
import com.qmetry.qaf.example.utility.CommonUtils;

public class HomeSteps extends WebDriverBaseTestPage<WebDriverTestPage> {

	@FindBy(locator = "travel.list")
	private List<QAFWebElement> travelList;

	HomePage homePage = new HomePage();
	WebDriverWait wait = new WebDriverWait(driver, 120);
	public static final String LOADER = "Login.Loader.img";

	@QAFTestStep(description = "after login user click on log out")
	public void clickLogOut() throws InterruptedException {
		homePage.clickOnLogOut();
	}

	@QAFTestStep(description = "after login user click on View All link in the Request section")
	public void clickOnViewAll() throws InterruptedException {
		// homePage.clickOnLogOut();
	}

	@QAFTestStep(description = "user click on manager view")
	public void switchManagerView() throws InterruptedException {
		waitForNotVisible(LOADER, 120);
		click("myview.slider");

	}

	@QAFTestStep(description = "user click on Travel tab for view {0}")
	public void clickTravelTab(String view) throws InterruptedException {
		waitForNotVisible(LOADER, 120);
		click("toogle.menu");
		CommonUtils.openSliderMenu("Travel", "Travel Requests", view);
		Thread.sleep(10000);

	}

	@QAFTestStep(description = "user click on pending record")
	public void clickRequestTravelTab() throws InterruptedException {
		for (int i = 0; i < travelList.size(); i++) {
			System.out.println("Nishant"+travelList.get(i).getText());
		}
	}

	@QAFTestStep(description = "user verify action approve and reject")
	public void verifyAction() throws InterruptedException {
      // CommonUtils.verifyPresent(" Approved");
       //CommonUtils.verifyPresent(" Rejected");
	}

	@Override
	protected void openPage(PageLocator locator, Object... args) {
		// TODO Auto-generated method stub

	}

}
