package com.qmetry.qaf.example.components;

import com.qmetry.qaf.automation.ui.webdriver.QAFWebComponent;

public class MenuComponent extends QAFWebComponent {

//	@FindBy(locator = NAME_LOC)
//	private QAFWebElement headerMenu;

	public MenuComponent(String locator) {
		super(locator);

	}

	// child component
	public class subMenu extends QAFWebComponent {
//		@FindBy(locator = NAME_LOC)
//		private QAFWebElement submenuMenu;

		public subMenu(String locator) {
			super(locator);

		}

	}

}
